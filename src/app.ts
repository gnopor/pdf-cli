import fs from "fs";
import { PDFDocument, PDFEmbeddedPage, PDFPage, degrees } from "pdf-lib";
import { Command } from "commander";
import figlet from "figlet";

// const usage = "\nUsage: -p <path_to_pdf_file>";
// const argv = yargs(process.argv.slice(2))
//     .usage(usage)
//     .option({
//         p: {
//             alias: "pdf",
//             describe: "Path to the PDF file",
//             type: "string",
//             demandOption: false
//         }
//     })
//     .help(true).argv;

console.log(figlet.textSync("GNOP PDF-CLI MF  ..."));

const program = new Command();
program
    .version("1.0.0")
    .description("A CLI to convert 2 protraits pages to  1 landscape page.")
    .option("-p, --pdf [path]", "Path to the pdf file")
    .option("-n, --number [boolean]", "Add page number")
    .parse(process.argv);
const options = program.opts();

if (options.pdf) {
    main();
}

async function main() {
    try {
        const pdfPath = options.pdf;
        const addNumber = options.number;

        if (!fs.existsSync(pdfPath)) throw new Error("File not found.");
        if (String(pdfPath).toLowerCase().slice(-3) != "pdf") {
            throw new Error("Enter a .pdf file.");
        }

        console.log(`\n\n\n\nStart processing your pdf: ${pdfPath}`);
        const newDocPath = await convertFromPortraitToLandscape(pdfPath, { addNumber });
        console.log(`Find your file here: ${newDocPath}`);
    } catch (error) {
        console.error(error);
    }
}

// --------------------------------------------------

async function convertFromPortraitToLandscape(filePath: string, options = { addNumber: true }) {
    const existingPdfBytes = fs.readFileSync(filePath);

    const oldPdfDoc = await PDFDocument.load(existingPdfBytes);
    const newPdfDoc = await PDFDocument.create();

    const pages = oldPdfDoc.getPages();
    for (let i = 0; i < pages.length; i += 2) {
        let embeddedPages1: PDFEmbeddedPage[] = [];
        let embeddedPages2: PDFEmbeddedPage[] = [];

        embeddedPages1 = await newPdfDoc.embedPdf(oldPdfDoc, [i]);
        if (i + 1 != pages.length) {
            embeddedPages2 = await newPdfDoc.embedPdf(oldPdfDoc, [i + 1]);
        }

        const page = newPdfDoc.addPage();
        options.addNumber && addNumber(page, i + 1);
        await mergePages(page, embeddedPages1[0], embeddedPages2[0]);
    }

    const splitedPath = filePath.split("/");
    const newFilePath = `merged_&_numered ${splitedPath[splitedPath.length - 1]}`;
    fs.writeFileSync(newFilePath, await newPdfDoc.save());
    return newFilePath;
}

async function mergePages(
    page: PDFPage,
    embeddedPage1: PDFEmbeddedPage,
    embeddedPage2: PDFEmbeddedPage
) {
    page.setSize(embeddedPage1.width, embeddedPage1.height);
    page.setRotation(degrees(-90));

    page.drawPage(embeddedPage1, {
        x: 0,
        y: page.getHeight(),
        width: page.getHeight() / 2,
        height: page.getWidth(),
        rotate: degrees(-90),
        opacity: 1
    });

    embeddedPage2 &&
        page.drawPage(embeddedPage2, {
            x: 0,
            y: page.getHeight() / 2,
            width: page.getHeight() / 2,
            height: page.getWidth(),
            rotate: degrees(-90),
            opacity: 1
        });
}

function addNumber(page: PDFPage, startNumber: number) {
    const baseStep = page.getHeight() / 4;

    page.drawText(String(startNumber), {
        x: 10,
        y: baseStep * 3,
        size: 10,
        rotate: degrees(-90)
    });

    page.drawText(String(startNumber + 1), {
        x: 10,
        y: baseStep,
        size: 10,
        rotate: degrees(-90)
    });
}
